grammar calculator;

// Parser
program
    : stat* EOF
    ;

stat
    : assigment
    | expr 
    ;
    
assigment
    : ID EQ expr SEMI
    ;

expr
    : unaryExpr
    | expr '^' expr
    | expr '*' expr
    | expr '/' expr
    | expr '+' expr
    | expr '-' expr
    | ID
    | NUMBER
    | '(' expr ')'
    ;

unaryExpr
    : '-' expr
    | '+' expr
    | '!' expr
    ;

// Lexer
MUL:     '*';
DIV:     '/';
ADD:     '+';
SUB:     '-';
POWER:   '^';
EQ:      '=';
NO:      '!';
COMMA : ',' ;
SEMI : ';' ;
LPAREN : '(' ;
RPAREN : ')' ;
LCURLY : '{' ;
RCURLY : '}' ;

ID:      [a-zA-Z_][a-zA-Z0-9_]*;
NUMBER:  ('0'..'9')+ ('.' ('0'..'9')*)?;
WS: [ \t\n\r\f]+ -> skip ;