# Implementacion de una calculadora en notación polaca con ANTLR4 en lenguaje C++ #

Para empezar con el proyecto lo primero es escribir la gramatica, que nos ayudara a detectar los tokens y nos servira para generar los archivos necesarios. Para probar nuestra gramatica usaremos [antlr lab](http://lab.antlr.org).

## Gramatica 

~~~
    grammar calculator;

    // Parser
    // definimos el programa que contendra 0 a mas statements y el EOF que es el final del archivo
    program
        : stat* EOF
        ;

    // un statement puede ser una asignacion o una expresion
    stat
        : assigment
        | expr 
        ;

    // una asignacion tiene la forma de un identificador '=' una expresion y al final un ';'
    assigment
        : ID EQ expr SEMI
        ;

    // una expresion puede ser una expresion de un solo numero o una operacion de 2 numeros o un identificador o un numero o una expresion entre parentesis
    expr
        : unaryExpr
        | expr '^' expr
        | expr '*' expr
        | expr '/' expr
        | expr '+' expr
        | expr '-' expr
        | ID
        | NUMBER
        | '(' expr ')'
        ;

    // una expresion de un solo  numero puede ser un numero negativo, positivo o la negacion 
    unaryExpr
        : '-' expr
        | '+' expr
        | '!' expr
        ;

    // Lexer
    // aqui definimos los simbolos y/o numeros a usar
    MUL:     '*';
    DIV:     '/';
    ADD:     '+';
    SUB:     '-';
    POWER:   '^';
    EQ:      '=';
    NO:      '!';
    COMMA : ',' ;
    SEMI : ';' ;
    LPAREN : '(' ;
    RPAREN : ')' ;
    LCURLY : '{' ;
    RCURLY : '}' ;

    ID:      [a-zA-Z_][a-zA-Z0-9_]*;
    NUMBER:  ('0'..'9')+ ('.' ('0'..'9')*)?;
    WS: [ \t\n\r\f]+ -> skip ;
~~~

## Instalacion de herramientas

Creamos el contenedor ubuntu en docker iterativamente

    'docker run -it –name compiladores ubuntu bash'

Instalamos las herramientas

~~~
    apt-get update
    apt-get install -y software-properties-common
    apt-get install -y wget unzip build-essential
    apt-get install -y default-jdk
    apt-get install nvim
    apt-get install g++
    apt-get install cmake
~~~

## Instalamos antlr4

Para instalar antlr4 primero nos ubicamos en la siguiente ruta

    'cd /usr/local/lib'

Luego instalamos antlr en la ubicacion

    'wget https://www.antlr.org/download/antlr-4.13.1-complete.jar'

Tenemos que abrir el archivo ~/.bashrc con un editor de codigo como neo vim y al final del archivo colocamos las variables de entorno que necesitamos

~~~
    export CLASSPATH=".:/usr/local/lib/antlr-4.13.1-complete.jar:$CLASSPATH"
    alias antlr4='java -jar /usr/local/lib/antlr-4.13.1-complete.jar'
    alias grun='java org.antlr.v4.gui.TestRig'
~~~

Podemos correr los siguientes comandos para verificar que todo este instalado correctamente

~~~
    java -version
    echo $CLASSPATH
    antlr4
~~~

## Instalamos el runtime de c++ para antlr4

Para descargar el runtime necesario para c++ nos vamos a la [pagina de antlr4](https://www.antlr.org/download.html) y descargamos el archivo.zip necesario.

    'antlr4-cpp-runtime-4.13.1-source.zip (.h, .cpp)'

Luego copiamos este archivo en nuestro contenedor, para esto nos ubicamos en el directorio de nuestro archivo descargado y lo copiamos al contenedor

    'docker cp antlr4-cpp-runtime-4.13.1-source.zip compiladores:/runtime_cpp_antlr'

Extraemos su contenido

    'unzip antlr4-cpp-runtime-4.13.1-source.zip'

Creamos un directorio build para los archivos necesarios para la instalacion y una carpeta run donde se instalara las librerias

    'mkdir build && mkdir run && cd build'

Creamos la build con cmake

    'cmake .. -DANTLR_JAR_LOCATION=/usr/local/lib/antlr-4.13.1-complete.jar -DWITH_DEMO=True'

La ejecutamos

    'make'

Instalamos las librerias en la carpeta run que creamos anteriormente

    'DESTDIR=/runtime_cpp_antlr/run make install'

## Generamos los archivos .cpp y .h segun la gramatica con ANTLR

Creamos un directorio donde pondremos nuestro archivo calculator.g4 que contiene la gramatica mencionada anteriormente

    'mkdir antlr_generated_files'

Creamos el archivo calculator.g4 y copiamos el codigo de la gramatica

Para generar los archivos con antlr4 hacemos lo siguiente

    'antlr4 -Dlanguage=Cpp calculator.g4'


## Creamos el codigo en c++ que unira todo

En el directorio donde estan los archivos generados por antlr4 creamos un proyecto en c++ llamado proyecto.cpp

    'nvim proyecto.cpp'

Este codigo nos permitira mediante un input procesarlo con el lexer y parser y nos permitira observar el arbol que se ha generado de acuerdo a la expresion en el input

~~~
   #include <iostream>
#include "antlr4-runtime.h"
#include "calculatorLexer.h"
#include "calculatorParser.h"


//IMPLEMENTACION DEL LISTENER
#include "MyCalculatorListener.h"
#include <any>
#include <map>
#include <string>


using namespace antlr4;
using namespace std;



int main() {
    //Se implementaron los usos de las funciones

    //ADD
    //DIV
    //MUL
    //SUB
    //POWER
    //EQ

    std::string inputExpression = "x = 5 + 3 * 2; y = x / 2;";

    // Cree un flujo de entrada basado en la cadena de texto
    ANTLRInputStream input(inputExpression);
    calculatorLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    // Cree un analizador sintáctico que procese los tokens producidos por el analizador léxico
    calculatorParser parser(&tokens);

    // Genere el árbol de análisis sintáctico llamando a la regla de inicio 'program'
    calculatorParser::ProgramContext* tree = parser.program();

    // Muestre la expresión del árbol de análisis sintáctico
    std::cout <<"Expresion del arbol de analisis sintatico:" <<endl<< tree->toStringTree(&parser) << std::endl;


    MyCalculatorListener listener;
    tree->enterRule(&listener);

    std::cout << "\n\nResult: " << listener.getResult() << std::endl;



    return 0;
}
~~~

Finalmente compilamos el codigo con la ubicacion del antlr-runtime.h, que esta en la carpeta run que creamos anteriormente

    'g++ -o calculator_exec proyecto.cpp -I<ruta de antlr-runtime.h>'