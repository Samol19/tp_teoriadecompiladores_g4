#include <iostream>
#include "antlr4-runtime.h"
#include "calculatorLexer.h"
#include "calculatorParser.h"


//IMPLEMENTACION DEL LISTENER
#include "MyCalculatorListener.h"
#include <any>
#include <map>
#include <string>


using namespace antlr4;
using namespace std;



int main() {
    //Se implementaron los usos de las funciones

    //ADD
    //DIV
    //MUL
    //SUB
    //POWER
    //EQ

    std::string inputExpression = "x = 5 + 3 * 2; y = x / 2;";

    // Cree un flujo de entrada basado en la cadena de texto
    ANTLRInputStream input(inputExpression);
    calculatorLexer lexer(&input);
    CommonTokenStream tokens(&lexer);

    // Cree un analizador sint�ctico que procese los tokens producidos por el analizador l�xico
    calculatorParser parser(&tokens);

    // Genere el �rbol de an�lisis sint�ctico llamando a la regla de inicio 'program'
    calculatorParser::ProgramContext* tree = parser.program();

    // Muestre la expresi�n del �rbol de an�lisis sint�ctico
    std::cout <<"Expresion del arbol de analisis sintatico:" <<endl<< tree->toStringTree(&parser) << std::endl;


    MyCalculatorListener listener;
    tree->enterRule(&listener);

    std::cout << "\n\nResult: " << listener.getResult() << std::endl;



    return 0;
}