
// Generated from calculator.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "calculatorParser.h"


/**
 * This interface defines an abstract listener for a parse tree produced by calculatorParser.
 */
class  calculatorListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterProgram(calculatorParser::ProgramContext *ctx) = 0;
  virtual void exitProgram(calculatorParser::ProgramContext *ctx) = 0;

  virtual void enterStat(calculatorParser::StatContext *ctx) = 0;
  virtual void exitStat(calculatorParser::StatContext *ctx) = 0;

  virtual void enterAssigment(calculatorParser::AssigmentContext *ctx) = 0;
  virtual void exitAssigment(calculatorParser::AssigmentContext *ctx) = 0;

  virtual void enterExpr(calculatorParser::ExprContext *ctx) = 0;
  virtual void exitExpr(calculatorParser::ExprContext *ctx) = 0;

  virtual void enterUnaryExpr(calculatorParser::UnaryExprContext *ctx) = 0;
  virtual void exitUnaryExpr(calculatorParser::UnaryExprContext *ctx) = 0;


};

