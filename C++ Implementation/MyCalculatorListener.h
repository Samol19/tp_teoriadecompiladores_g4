#pragma once

#include "antlr4-runtime.h"
#include "calculatorBaseListener.h"
#include <stack>
#include <map>
#include <string>

class MyCalculatorListener : public calculatorBaseListener {
public:
    void enterAssigment(calculatorParser::AssigmentContext* ctx) override {
        std::string id = ctx->ID()->getText();
        double value = evaluateExpression(ctx->expr());
        variables[id] = value; // Almacena el valor de la expresión en la variable
    }

    double getResult() const {
        return result;
    }

private:
    std::map<std::string, double> variables; // Almacena las variables y sus valores
    double result; // Almacena el resultado final de la expresión

    double evaluateExpression(calculatorParser::ExprContext* ctx) {
        double left = evaluateOperand(ctx->expr(0));
        double right = evaluateOperand(ctx->expr(1));

        if (ctx->ADD()) {
            return left + right;
        }
        else if (ctx->SUB()) {
            return left - right;
        }
        else if (ctx->MUL()) {
            return left * right;
        }
        else if (ctx->DIV()) {
            return left / right;
        }
        else if (ctx->POWER()) {
            return pow(left, right);
        }
        else if (ctx->NUMBER()) {
            return std::stod(ctx->NUMBER()->getText());
        }
        else if (ctx->ID()) {
            std::string id = ctx->ID()->getText();
            if (variables.find(id) != variables.end()) {
                return variables[id];
            }
        }

        return 0.0;
    }

    double evaluateOperand(calculatorParser::ExprContext* ctx) {
        if (ctx) {
            return evaluateExpression(ctx);
        }
        return 0.0;
    }
};