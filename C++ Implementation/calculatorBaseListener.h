
// Generated from calculator.g4 by ANTLR 4.9.2

#pragma once


#include "antlr4-runtime.h"
#include "calculatorListener.h"


/**
 * This class provides an empty implementation of calculatorListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
	class  calculatorBaseListener : public calculatorListener {
	public:

	  virtual void enterProgram(calculatorParser::ProgramContext * /*ctx*/) override { }
	  virtual void exitProgram(calculatorParser::ProgramContext * /*ctx*/) override { }

	  virtual void enterStat(calculatorParser::StatContext * /*ctx*/) override { }
	  virtual void exitStat(calculatorParser::StatContext * /*ctx*/) override { }

	  virtual void enterAssigment(calculatorParser::AssigmentContext * /*ctx*/) override { }
	  virtual void exitAssigment(calculatorParser::AssigmentContext * /*ctx*/) override { }

	  virtual void enterExpr(calculatorParser::ExprContext * /*ctx*/) override { }
	  virtual void exitExpr(calculatorParser::ExprContext * /*ctx*/) override { }

	  virtual void enterUnaryExpr(calculatorParser::UnaryExprContext * /*ctx*/) override { }
	  virtual void exitUnaryExpr(calculatorParser::UnaryExprContext * /*ctx*/) override { }


	  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
	  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
	  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
	  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

	};

