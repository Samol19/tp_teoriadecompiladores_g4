// Generated from calculator.g4 by ANTLR 4.9.2
#pragma once

#include "antlr4-runtime.h"
#include "calculatorParser.h"



/**
 * This class defines an abstract visitor for a parse tree
 * produced by calculatorParser.
 */
class  calculatorVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by calculatorParser.
   */
    virtual antlrcpp::Any visitProgram(calculatorParser::ProgramContext *context) = 0;

    virtual antlrcpp::Any visitStat(calculatorParser::StatContext *context) = 0;

    virtual antlrcpp::Any visitAssigment(calculatorParser::AssigmentContext *context) = 0;

    virtual antlrcpp::Any visitExpr(calculatorParser::ExprContext *context) = 0;

    virtual antlrcpp::Any visitUnaryExpr(calculatorParser::UnaryExprContext *context) = 0;


};

