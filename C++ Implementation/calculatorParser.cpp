
// Generated from calculator.g4 by ANTLR 4.9.2


#include "calculatorListener.h"

#include "calculatorParser.h"


using namespace antlrcpp;
using namespace antlr4;

calculatorParser::calculatorParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

calculatorParser::~calculatorParser() {
  delete _interpreter;
}

std::string calculatorParser::getGrammarFileName() const {
  return "calculator.g4";
}

const std::vector<std::string>& calculatorParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& calculatorParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- ProgramContext ------------------------------------------------------------------

calculatorParser::ProgramContext::ProgramContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* calculatorParser::ProgramContext::EOF() {
  return getToken(calculatorParser::EOF, 0);
}

std::vector<calculatorParser::StatContext *> calculatorParser::ProgramContext::stat() {
  return getRuleContexts<calculatorParser::StatContext>();
}

calculatorParser::StatContext* calculatorParser::ProgramContext::stat(size_t i) {
  return getRuleContext<calculatorParser::StatContext>(i);
}


size_t calculatorParser::ProgramContext::getRuleIndex() const {
  return calculatorParser::RuleProgram;
}

void calculatorParser::ProgramContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterProgram(this);
}

void calculatorParser::ProgramContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitProgram(this);
}

calculatorParser::ProgramContext* calculatorParser::program() {
  ProgramContext *_localctx = _tracker.createInstance<ProgramContext>(_ctx, getState());
  enterRule(_localctx, 0, calculatorParser::RuleProgram);
  size_t _la = 0;

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(13);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while ((((_la & ~ 0x3fULL) == 0) &&
      ((1ULL << _la) & ((1ULL << calculatorParser::ADD)
      | (1ULL << calculatorParser::SUB)
      | (1ULL << calculatorParser::NO)
      | (1ULL << calculatorParser::LPAREN)
      | (1ULL << calculatorParser::ID)
      | (1ULL << calculatorParser::NUMBER))) != 0)) {
      setState(10);
      stat();
      setState(15);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
    setState(16);
    match(calculatorParser::EOF);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- StatContext ------------------------------------------------------------------

calculatorParser::StatContext::StatContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

calculatorParser::AssigmentContext* calculatorParser::StatContext::assigment() {
  return getRuleContext<calculatorParser::AssigmentContext>(0);
}

calculatorParser::ExprContext* calculatorParser::StatContext::expr() {
  return getRuleContext<calculatorParser::ExprContext>(0);
}


size_t calculatorParser::StatContext::getRuleIndex() const {
  return calculatorParser::RuleStat;
}

void calculatorParser::StatContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterStat(this);
}

void calculatorParser::StatContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitStat(this);
}

calculatorParser::StatContext* calculatorParser::stat() {
  StatContext *_localctx = _tracker.createInstance<StatContext>(_ctx, getState());
  enterRule(_localctx, 2, calculatorParser::RuleStat);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(20);
    _errHandler->sync(this);
    switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 1, _ctx)) {
    case 1: {
      enterOuterAlt(_localctx, 1);
      setState(18);
      assigment();
      break;
    }

    case 2: {
      enterOuterAlt(_localctx, 2);
      setState(19);
      expr(0);
      break;
    }

    default:
      break;
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- AssigmentContext ------------------------------------------------------------------

calculatorParser::AssigmentContext::AssigmentContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* calculatorParser::AssigmentContext::ID() {
  return getToken(calculatorParser::ID, 0);
}

tree::TerminalNode* calculatorParser::AssigmentContext::EQ() {
  return getToken(calculatorParser::EQ, 0);
}

calculatorParser::ExprContext* calculatorParser::AssigmentContext::expr() {
  return getRuleContext<calculatorParser::ExprContext>(0);
}

tree::TerminalNode* calculatorParser::AssigmentContext::SEMI() {
  return getToken(calculatorParser::SEMI, 0);
}


size_t calculatorParser::AssigmentContext::getRuleIndex() const {
  return calculatorParser::RuleAssigment;
}

void calculatorParser::AssigmentContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAssigment(this);
}

void calculatorParser::AssigmentContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAssigment(this);
}

calculatorParser::AssigmentContext* calculatorParser::assigment() {
  AssigmentContext *_localctx = _tracker.createInstance<AssigmentContext>(_ctx, getState());
  enterRule(_localctx, 4, calculatorParser::RuleAssigment);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(22);
    match(calculatorParser::ID);
    setState(23);
    match(calculatorParser::EQ);
    setState(24);
    expr(0);
    setState(25);
    match(calculatorParser::SEMI);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- ExprContext ------------------------------------------------------------------

calculatorParser::ExprContext::ExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

calculatorParser::UnaryExprContext* calculatorParser::ExprContext::unaryExpr() {
  return getRuleContext<calculatorParser::UnaryExprContext>(0);
}

tree::TerminalNode* calculatorParser::ExprContext::ID() {
  return getToken(calculatorParser::ID, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::NUMBER() {
  return getToken(calculatorParser::NUMBER, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::LPAREN() {
  return getToken(calculatorParser::LPAREN, 0);
}

std::vector<calculatorParser::ExprContext *> calculatorParser::ExprContext::expr() {
  return getRuleContexts<calculatorParser::ExprContext>();
}

calculatorParser::ExprContext* calculatorParser::ExprContext::expr(size_t i) {
  return getRuleContext<calculatorParser::ExprContext>(i);
}

tree::TerminalNode* calculatorParser::ExprContext::RPAREN() {
  return getToken(calculatorParser::RPAREN, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::POWER() {
  return getToken(calculatorParser::POWER, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::MUL() {
  return getToken(calculatorParser::MUL, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::DIV() {
  return getToken(calculatorParser::DIV, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::ADD() {
  return getToken(calculatorParser::ADD, 0);
}

tree::TerminalNode* calculatorParser::ExprContext::SUB() {
  return getToken(calculatorParser::SUB, 0);
}


size_t calculatorParser::ExprContext::getRuleIndex() const {
  return calculatorParser::RuleExpr;
}

void calculatorParser::ExprContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterExpr(this);
}

void calculatorParser::ExprContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitExpr(this);
}


calculatorParser::ExprContext* calculatorParser::expr() {
   return expr(0);
}

calculatorParser::ExprContext* calculatorParser::expr(int precedence) {
  ParserRuleContext *parentContext = _ctx;
  size_t parentState = getState();
  calculatorParser::ExprContext *_localctx = _tracker.createInstance<ExprContext>(_ctx, parentState);
  calculatorParser::ExprContext *previousContext = _localctx;
  (void)previousContext; // Silence compiler, in case the context is not used by generated code.
  size_t startState = 6;
  enterRecursionRule(_localctx, 6, calculatorParser::RuleExpr, precedence);

    

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    unrollRecursionContexts(parentContext);
  });
  try {
    size_t alt;
    enterOuterAlt(_localctx, 1);
    setState(35);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case calculatorParser::ADD:
      case calculatorParser::SUB:
      case calculatorParser::NO: {
        setState(28);
        unaryExpr();
        break;
      }

      case calculatorParser::ID: {
        setState(29);
        match(calculatorParser::ID);
        break;
      }

      case calculatorParser::NUMBER: {
        setState(30);
        match(calculatorParser::NUMBER);
        break;
      }

      case calculatorParser::LPAREN: {
        setState(31);
        match(calculatorParser::LPAREN);
        setState(32);
        expr(0);
        setState(33);
        match(calculatorParser::RPAREN);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
    _ctx->stop = _input->LT(-1);
    setState(54);
    _errHandler->sync(this);
    alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    while (alt != 2 && alt != atn::ATN::INVALID_ALT_NUMBER) {
      if (alt == 1) {
        if (!_parseListeners.empty())
          triggerExitRuleEvent();
        previousContext = _localctx;
        setState(52);
        _errHandler->sync(this);
        switch (getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 3, _ctx)) {
        case 1: {
          _localctx = _tracker.createInstance<ExprContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleExpr);
          setState(37);

          if (!(precpred(_ctx, 8))) throw FailedPredicateException(this, "precpred(_ctx, 8)");
          setState(38);
          match(calculatorParser::POWER);
          setState(39);
          expr(9);
          break;
        }

        case 2: {
          _localctx = _tracker.createInstance<ExprContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleExpr);
          setState(40);

          if (!(precpred(_ctx, 7))) throw FailedPredicateException(this, "precpred(_ctx, 7)");
          setState(41);
          match(calculatorParser::MUL);
          setState(42);
          expr(8);
          break;
        }

        case 3: {
          _localctx = _tracker.createInstance<ExprContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleExpr);
          setState(43);

          if (!(precpred(_ctx, 6))) throw FailedPredicateException(this, "precpred(_ctx, 6)");
          setState(44);
          match(calculatorParser::DIV);
          setState(45);
          expr(7);
          break;
        }

        case 4: {
          _localctx = _tracker.createInstance<ExprContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleExpr);
          setState(46);

          if (!(precpred(_ctx, 5))) throw FailedPredicateException(this, "precpred(_ctx, 5)");
          setState(47);
          match(calculatorParser::ADD);
          setState(48);
          expr(6);
          break;
        }

        case 5: {
          _localctx = _tracker.createInstance<ExprContext>(parentContext, parentState);
          pushNewRecursionContext(_localctx, startState, RuleExpr);
          setState(49);

          if (!(precpred(_ctx, 4))) throw FailedPredicateException(this, "precpred(_ctx, 4)");
          setState(50);
          match(calculatorParser::SUB);
          setState(51);
          expr(5);
          break;
        }

        default:
          break;
        } 
      }
      setState(56);
      _errHandler->sync(this);
      alt = getInterpreter<atn::ParserATNSimulator>()->adaptivePredict(_input, 4, _ctx);
    }
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }
  return _localctx;
}

//----------------- UnaryExprContext ------------------------------------------------------------------

calculatorParser::UnaryExprContext::UnaryExprContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* calculatorParser::UnaryExprContext::SUB() {
  return getToken(calculatorParser::SUB, 0);
}

calculatorParser::ExprContext* calculatorParser::UnaryExprContext::expr() {
  return getRuleContext<calculatorParser::ExprContext>(0);
}

tree::TerminalNode* calculatorParser::UnaryExprContext::ADD() {
  return getToken(calculatorParser::ADD, 0);
}

tree::TerminalNode* calculatorParser::UnaryExprContext::NO() {
  return getToken(calculatorParser::NO, 0);
}


size_t calculatorParser::UnaryExprContext::getRuleIndex() const {
  return calculatorParser::RuleUnaryExpr;
}

void calculatorParser::UnaryExprContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterUnaryExpr(this);
}

void calculatorParser::UnaryExprContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<calculatorListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitUnaryExpr(this);
}

calculatorParser::UnaryExprContext* calculatorParser::unaryExpr() {
  UnaryExprContext *_localctx = _tracker.createInstance<UnaryExprContext>(_ctx, getState());
  enterRule(_localctx, 8, calculatorParser::RuleUnaryExpr);

#if __cplusplus > 201703L
  auto onExit = finally([=, this] {
#else
  auto onExit = finally([=] {
#endif
    exitRule();
  });
  try {
    setState(63);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case calculatorParser::SUB: {
        enterOuterAlt(_localctx, 1);
        setState(57);
        match(calculatorParser::SUB);
        setState(58);
        expr(0);
        break;
      }

      case calculatorParser::ADD: {
        enterOuterAlt(_localctx, 2);
        setState(59);
        match(calculatorParser::ADD);
        setState(60);
        expr(0);
        break;
      }

      case calculatorParser::NO: {
        enterOuterAlt(_localctx, 3);
        setState(61);
        match(calculatorParser::NO);
        setState(62);
        expr(0);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

bool calculatorParser::sempred(RuleContext *context, size_t ruleIndex, size_t predicateIndex) {
  switch (ruleIndex) {
    case 3: return exprSempred(dynamic_cast<ExprContext *>(context), predicateIndex);

  default:
    break;
  }
  return true;
}

bool calculatorParser::exprSempred(ExprContext *_localctx, size_t predicateIndex) {
  switch (predicateIndex) {
    case 0: return precpred(_ctx, 8);
    case 1: return precpred(_ctx, 7);
    case 2: return precpred(_ctx, 6);
    case 3: return precpred(_ctx, 5);
    case 4: return precpred(_ctx, 4);

  default:
    break;
  }
  return true;
}

// Static vars and initialization.
std::vector<dfa::DFA> calculatorParser::_decisionToDFA;
atn::PredictionContextCache calculatorParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN calculatorParser::_atn;
std::vector<uint16_t> calculatorParser::_serializedATN;

std::vector<std::string> calculatorParser::_ruleNames = {
  "program", "stat", "assigment", "expr", "unaryExpr"
};

std::vector<std::string> calculatorParser::_literalNames = {
  "", "'*'", "'/'", "'+'", "'-'", "'^'", "'='", "'!'", "','", "';'", "'('", 
  "')'", "'{'", "'}'"
};

std::vector<std::string> calculatorParser::_symbolicNames = {
  "", "MUL", "DIV", "ADD", "SUB", "POWER", "EQ", "NO", "COMMA", "SEMI", 
  "LPAREN", "RPAREN", "LCURLY", "RCURLY", "ID", "NUMBER", "WS"
};

dfa::Vocabulary calculatorParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> calculatorParser::_tokenNames;

calculatorParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  static const uint16_t serializedATNSegment0[] = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
       0x3, 0x12, 0x44, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 
       0x9, 0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x3, 0x2, 0x7, 
       0x2, 0xe, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x11, 0xb, 0x2, 0x3, 0x2, 
       0x3, 0x2, 0x3, 0x3, 0x3, 0x3, 0x5, 0x3, 0x17, 0xa, 0x3, 0x3, 0x4, 
       0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 0x5, 0x3, 0x5, 0x3, 
       0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x5, 0x5, 
       0x26, 0xa, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 
       0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 
       0x5, 0x3, 0x5, 0x3, 0x5, 0x3, 0x5, 0x7, 0x5, 0x37, 0xa, 0x5, 0xc, 
       0x5, 0xe, 0x5, 0x3a, 0xb, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x3, 
       0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x42, 0xa, 0x6, 0x3, 0x6, 0x2, 
       0x3, 0x8, 0x7, 0x2, 0x4, 0x6, 0x8, 0xa, 0x2, 0x2, 0x2, 0x4a, 0x2, 
       0xf, 0x3, 0x2, 0x2, 0x2, 0x4, 0x16, 0x3, 0x2, 0x2, 0x2, 0x6, 0x18, 
       0x3, 0x2, 0x2, 0x2, 0x8, 0x25, 0x3, 0x2, 0x2, 0x2, 0xa, 0x41, 0x3, 
       0x2, 0x2, 0x2, 0xc, 0xe, 0x5, 0x4, 0x3, 0x2, 0xd, 0xc, 0x3, 0x2, 
       0x2, 0x2, 0xe, 0x11, 0x3, 0x2, 0x2, 0x2, 0xf, 0xd, 0x3, 0x2, 0x2, 
       0x2, 0xf, 0x10, 0x3, 0x2, 0x2, 0x2, 0x10, 0x12, 0x3, 0x2, 0x2, 0x2, 
       0x11, 0xf, 0x3, 0x2, 0x2, 0x2, 0x12, 0x13, 0x7, 0x2, 0x2, 0x3, 0x13, 
       0x3, 0x3, 0x2, 0x2, 0x2, 0x14, 0x17, 0x5, 0x6, 0x4, 0x2, 0x15, 0x17, 
       0x5, 0x8, 0x5, 0x2, 0x16, 0x14, 0x3, 0x2, 0x2, 0x2, 0x16, 0x15, 0x3, 
       0x2, 0x2, 0x2, 0x17, 0x5, 0x3, 0x2, 0x2, 0x2, 0x18, 0x19, 0x7, 0x10, 
       0x2, 0x2, 0x19, 0x1a, 0x7, 0x8, 0x2, 0x2, 0x1a, 0x1b, 0x5, 0x8, 0x5, 
       0x2, 0x1b, 0x1c, 0x7, 0xb, 0x2, 0x2, 0x1c, 0x7, 0x3, 0x2, 0x2, 0x2, 
       0x1d, 0x1e, 0x8, 0x5, 0x1, 0x2, 0x1e, 0x26, 0x5, 0xa, 0x6, 0x2, 0x1f, 
       0x26, 0x7, 0x10, 0x2, 0x2, 0x20, 0x26, 0x7, 0x11, 0x2, 0x2, 0x21, 
       0x22, 0x7, 0xc, 0x2, 0x2, 0x22, 0x23, 0x5, 0x8, 0x5, 0x2, 0x23, 0x24, 
       0x7, 0xd, 0x2, 0x2, 0x24, 0x26, 0x3, 0x2, 0x2, 0x2, 0x25, 0x1d, 0x3, 
       0x2, 0x2, 0x2, 0x25, 0x1f, 0x3, 0x2, 0x2, 0x2, 0x25, 0x20, 0x3, 0x2, 
       0x2, 0x2, 0x25, 0x21, 0x3, 0x2, 0x2, 0x2, 0x26, 0x38, 0x3, 0x2, 0x2, 
       0x2, 0x27, 0x28, 0xc, 0xa, 0x2, 0x2, 0x28, 0x29, 0x7, 0x7, 0x2, 0x2, 
       0x29, 0x37, 0x5, 0x8, 0x5, 0xb, 0x2a, 0x2b, 0xc, 0x9, 0x2, 0x2, 0x2b, 
       0x2c, 0x7, 0x3, 0x2, 0x2, 0x2c, 0x37, 0x5, 0x8, 0x5, 0xa, 0x2d, 0x2e, 
       0xc, 0x8, 0x2, 0x2, 0x2e, 0x2f, 0x7, 0x4, 0x2, 0x2, 0x2f, 0x37, 0x5, 
       0x8, 0x5, 0x9, 0x30, 0x31, 0xc, 0x7, 0x2, 0x2, 0x31, 0x32, 0x7, 0x5, 
       0x2, 0x2, 0x32, 0x37, 0x5, 0x8, 0x5, 0x8, 0x33, 0x34, 0xc, 0x6, 0x2, 
       0x2, 0x34, 0x35, 0x7, 0x6, 0x2, 0x2, 0x35, 0x37, 0x5, 0x8, 0x5, 0x7, 
       0x36, 0x27, 0x3, 0x2, 0x2, 0x2, 0x36, 0x2a, 0x3, 0x2, 0x2, 0x2, 0x36, 
       0x2d, 0x3, 0x2, 0x2, 0x2, 0x36, 0x30, 0x3, 0x2, 0x2, 0x2, 0x36, 0x33, 
       0x3, 0x2, 0x2, 0x2, 0x37, 0x3a, 0x3, 0x2, 0x2, 0x2, 0x38, 0x36, 0x3, 
       0x2, 0x2, 0x2, 0x38, 0x39, 0x3, 0x2, 0x2, 0x2, 0x39, 0x9, 0x3, 0x2, 
       0x2, 0x2, 0x3a, 0x38, 0x3, 0x2, 0x2, 0x2, 0x3b, 0x3c, 0x7, 0x6, 0x2, 
       0x2, 0x3c, 0x42, 0x5, 0x8, 0x5, 0x2, 0x3d, 0x3e, 0x7, 0x5, 0x2, 0x2, 
       0x3e, 0x42, 0x5, 0x8, 0x5, 0x2, 0x3f, 0x40, 0x7, 0x9, 0x2, 0x2, 0x40, 
       0x42, 0x5, 0x8, 0x5, 0x2, 0x41, 0x3b, 0x3, 0x2, 0x2, 0x2, 0x41, 0x3d, 
       0x3, 0x2, 0x2, 0x2, 0x41, 0x3f, 0x3, 0x2, 0x2, 0x2, 0x42, 0xb, 0x3, 
       0x2, 0x2, 0x2, 0x8, 0xf, 0x16, 0x25, 0x36, 0x38, 0x41, 
  };

  _serializedATN.insert(_serializedATN.end(), serializedATNSegment0,
    serializedATNSegment0 + sizeof(serializedATNSegment0) / sizeof(serializedATNSegment0[0]));


  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

calculatorParser::Initializer calculatorParser::_init;
